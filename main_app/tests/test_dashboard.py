from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token
from main_app.models import UserProfile,Opportunities
class UserProfileTestCase(APITestCase):
    url = '/user_profile/'
    def setUp(self):
        self.username = "admin"
        self.password = "adminadmin"
        self.user = User.objects.create_user(username=self.username, password=self.password)
        self.auth_token = Token.objects.get(user=self.user)

        self.user_profile_obj = UserProfile.objects.create(credit=1,image='test.jpg',user=self.user, name='thiyagu')

        #user with full profile completion
        self.username = "admin2"
        self.password = "adminadmin2"
        self.user2 = User.objects.create_user(username=self.username, password=self.password)
        self.auth_token2 = Token.objects.get(user=self.user2)

        self.user_profile_obj2 = UserProfile.objects.create(credit=1, image='test.jpg', user=self.user2, name='thiyagu',email = 'test@gmail.com', phone= '9894630300')

        #user with zero profile completeness
        # user with full profile completion
        self.username = "admin3"
        self.password = "adminadmin3"
        self.user3 = User.objects.create_user(username=self.username, password=self.password)
        self.auth_token3 = Token.objects.get(user=self.user3)

        self.user_profile_obj2 = UserProfile.objects.create(credit=1, user=self.user3)

    def test_partial_profile_completeness(self):

        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token), format='json')
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['data']['profile_completion'],50)

    def test_full_profile_completeness(self):

        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token2), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['data']['profile_completion'],100)

    def test_zero_profile_completeness(self):

        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token3), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['data']['profile_completion'],0)

class OpportunityTestCase(APITestCase):
    url = '/opportunities/'
    def setUp(self):
        self.username = "admin"
        self.password = "adminadmin"
        self.user = User.objects.create_user(username=self.username, password=self.password)
        self.auth_token = Token.objects.get(user=self.user)

        self.user_profile_obj = UserProfile.objects.create(credit=1,image='test.jpg',user=self.user, name='thiyagu')

        self.opportunity1 = Opportunities.objects.create(title = 'test', image = 'test.jpg', is_featured= True)
        self.opportunity2 = Opportunities.objects.create(title = 'test2', image = 'test.jpg', is_featured= False)

    def test_successful_oppurtunity(self):
        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {token}'.format(token=self.auth_token), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


