from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token

class UserLoginTestCase(APITestCase):
    url = '/login/'
    admin_token=''
    def setUp(self):
        self.username = "admin"
        self.password = "adminadmin"
        self.email = "admin@admin.com"
        self.user = User.objects.create_user(username=self.username, password=self.password, email=self.email)
        self.auth_token = Token.objects.get(user=self.user)
        self.admin_token=self.auth_token.key


    def test_successful_login(self):
        data = {
            'username': 'admin',
            'password': 'adminadmin'
        }

        response = self.client.post(self.url, data,  format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.count(), 1)

    def test_login_invalid_credemtials(self):
        data = {
            'username': 'admin',
            'password': 'admiadmin'
        }

        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_missing_credentials(self):
        data = {
            'username': 'admin',
            'password': 'admiadmin'
        }

        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)