from .models import UserProfile, Opportunities
from buildingblocks.settings import DOMAIN, STATIC_URL


def opportunity_controller():
    """
    :return: list of all opportunities
    """

    opportunity_obj = Opportunities.objects.all()

    try:
        featured_opp = \
        opportunity_obj.filter(is_featured=True).values('title', 'caption', 'additional_offers', 'description',
                                                        'image')[0]
        featured_opp['image'] = form_image_abs_url(featured_opp['image'])
    except Exception as e:
        print("No featured Opportunities found")
        featured_opp = {}

    # getting other opportunities
    other_opp = opportunity_obj.filter(is_featured=False).values('title', 'caption', 'description', 'image')

    for opp in other_opp:
        opp['image'] = form_image_abs_url(opp['image'])

    return {"featured": featured_opp, "other": other_opp}


def profile_info_controller(user):
    """

    :param user:User object
    :return: profile info
    """
    profile_dict = UserProfile.objects.filter(user=user).values('credit', 'image', 'email', 'phone', 'name')[0]

    if profile_dict['image']:
        profile_dict['image'] = form_image_abs_url(profile_dict['image'])

    # calculating profile completeness %
    profile_values_list = list(profile_dict.values())
    profile_dict['profile_completion'] = 100 - (25 * (profile_values_list.count(None) + profile_values_list.count('')))
    return profile_dict

def form_image_abs_url(path):
    return (DOMAIN + STATIC_URL + path)  # Since serving images in django static server