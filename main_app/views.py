from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken import views
from rest_framework.renderers import TemplateHTMLRenderer,JSONRenderer
from rest_framework.decorators import api_view, permission_classes, renderer_classes,authentication_classes
from rest_framework.permissions import IsAuthenticated
from .controllers import opportunity_controller, profile_info_controller
from rest_framework.authtoken.models import Token
from django.http import HttpResponseRedirect
from django.contrib.auth import login
from rest_framework.authentication import SessionAuthentication, BasicAuthentication


class LoginView(APIView):
    """
    Login the user
    """
    renderer_classes = [JSONRenderer, TemplateHTMLRenderer]

    def get(self, request):
        return Response(template_name = 'login.html')

    def post(self, request):
        """
        :return: auth_token for application content type else Login  or Dashboard HTML for text/html content type
        """
        auth_response = views.obtain_auth_token(request._request)
        content_type = request.META.get('CONTENT_TYPE')

        if content_type == 'application/json' or content_type == 'application/json; charset=None':
         return auth_response
        elif auth_response.status_code==200:
            user = Token.objects.get(key=auth_response.data['token']).user
            login(request, user)
            return HttpResponseRedirect('/dashboard/')
        else:
            return Response({'error': "Incorrect username or password"},template_name='login.html')

@api_view(['GET'])
@authentication_classes((SessionAuthentication,BasicAuthentication))
@permission_classes((IsAuthenticated,))
@renderer_classes((TemplateHTMLRenderer,))
def dashboard(request):
    profile_info = profile_info_controller(request.user)
    opportunities = opportunity_controller()
    return Response({'opportunities': opportunities,'profile':profile_info},template_name='dashboard.html')


#API to fetch profile info
@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def user_profile(request):

    try:
        profile_dict = profile_info_controller(request.user)
        return Response({"data":profile_dict,"status":"success"})
    except Exception as e:
        print(e)
        return Response({"error":"Some error occured. Please try again"},status=400)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def opportunities(request):
    """
        Fetches all opportunities
    """
    try:
        result = opportunity_controller()
        return Response({"data":result,"status":"success"})
    except Exception as e:
        print(e)
        return Response({"error":"Some error occured. Please try again"},status=400)


