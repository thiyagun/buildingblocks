from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

#Create token on user creation
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Opportunities(models.Model):
    title = models.CharField(max_length=15)
    caption = models.CharField(max_length=40)
    additional_offers = models.CharField(max_length=40, blank=True, null=True)
    description = models.TextField(max_length=200,blank=True,null=True)
    is_featured = models.BooleanField(default=False)
    image= models.ImageField(upload_to='img/opportunity')

class UserProfile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    credit = models.DecimalField(default=0,decimal_places=2, max_digits=6)
    image= models.ImageField(upload_to='img/userprofile',blank=True,null=True)
    email = models.EmailField(blank=True,null=True)
    phone = models.CharField(max_length=10,blank=True,null=True)
    name = models.CharField(max_length=20,blank=True,null=True)
