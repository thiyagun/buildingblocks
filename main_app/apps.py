from django.apps import AppConfig


class BuildingblockConfig(AppConfig):
    name = 'main_app'
