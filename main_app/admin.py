from django.contrib import admin
from .models import UserProfile,Opportunities


class UserProfileAdmin(admin.ModelAdmin):
    list_display =['user','credit']

class OpportunitiesAdmin(admin.ModelAdmin):
    list_display = ['title', 'caption','is_featured']

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Opportunities, OpportunitiesAdmin)