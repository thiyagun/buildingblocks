from main_app.views import LoginView,user_profile, opportunities, dashboard
from django.conf.urls import url

urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^user_profile/$', user_profile, name='user_profile'),
    url(r'^opportunities/$', opportunities, name='opportunities'),
    url(r'^dashboard/$', dashboard, name='opportunities'),

]